import React, { useState, useEffect } from "react";
import { BowlerData } from "../../actions";
import { useSelector, useDispatch } from "react-redux";
import PlayerList from "../../pages/playerList";

const Bowler = () => {
    const dispatch = useDispatch();
    const player = useSelector((state) => state.player.player);
    const [data, setData] = useState([]);

    useEffect(() => {

        dispatch(BowlerData());
        
    }, [dispatch]);

    useEffect(() => {
        setData(player);
    }, [player]);

    console.log(player);
    console.log(data);

    return (
        <div>
            <PlayerList data={data} />
        </div>
    );
}

export default Bowler;