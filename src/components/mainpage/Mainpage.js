import React from "react";
import Showdata from "../Showdata/Showdata";
import "./Mainpage.css";

const Mainpage = () => {
  let newDate = new Date();
  let date = newDate.getDate();
  // let month = newDate.getMonth() + 1;
  let year = newDate.getFullYear();
  let months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  let monthName = months[newDate.getMonth()];

   var today = new Date(),
 
    currentTime = today.getHours() + ':' + today.getMinutes() ;
 


  return (
    <div className="start_bar">
      <div className="main">
        <div className="header">
          <div className="hed_title">
            <div className="logo">
              <i className="fa-solid fa-house"></i>
            </div>
            <div className="title">
              <h4>DC vs RR</h4>
              <p className="date">
                {monthName} {date}, {year} {currentTime} AM
              </p>
            </div>
          </div>
          <div className="desc">
            <p>Maximum 7 players from a team</p>
          </div>
          <div className="sub_main_parts">
            <h4>
              players <p>00/11</p>
            </h4>
            <h4>
              DC <p>0</p>
            </h4>
            <h4>
              RR <p>0</p>
            </h4>
            <h4>
              Credits Left <p className="point">100</p>
            </h4>
          </div>
          <div className="credits">
            <hr></hr>
          </div>
        </div>
      </div>
      <div className="main_data_display">
        <Showdata />
      </div>
      <div className="last_part">
        <button className="btn">Next</button>
      </div>
    </div>
  );
};

export default Mainpage;
