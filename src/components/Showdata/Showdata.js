import React from "react";
import "./Showdata.css";
import { Link } from "react-router-dom";
const Showdata = () => {
  return (
    <div className="main_tab">
      <div className="players_roles">
        {/* <h4>
          {" "}
          <Link to="/">WK(0)</Link>
        </h4>
        <h4>ALLR(0)</h4>
        <h4>BATS(0)</h4>
        <h4>BWL(0)</h4> */}
        <Link to="/">WK(0)</Link>
        <Link to="/allr">ALLR(0)</Link>
        <Link to="/bats">BATS(0)</Link>
        <Link to="/bwl">BWL(0)</Link>
      </div>
      <div className="select_items_maxmin">
        <p>Select (1-3) WK</p>
      </div>
    </div>
  );
};

export default Showdata;
