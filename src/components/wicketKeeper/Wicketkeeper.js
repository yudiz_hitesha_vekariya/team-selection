import React, { useState, useEffect } from "react";
import { WicketKeeperData } from "../../actions";
import { useDispatch, useSelector } from "react-redux";
import PlayerList from "../../pages/playerList";

const WicketKeeper = () => {
  const dispatch = useDispatch();
  const player = useSelector((state) => state.player.player);
  const [data, setData] = useState([]);

  useEffect(() => {
    dispatch(WicketKeeperData());
  }, [dispatch]);

  useEffect(() => {
    setData(player);
  }, [player]);

  // console.log(player);
  // console.log(data);

  return (
    <div>
      <PlayerList data={data} />
    </div>
  );
};

export default WicketKeeper;
