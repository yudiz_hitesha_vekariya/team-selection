import React, { useEffect, useState } from "react";
import { BatsManData } from "../../actions";
import PlayerList from "../../pages/playerList";
import { useSelector, useDispatch } from "react-redux";

const BatsMan = () => {
    const dispatch = useDispatch();
    const player = useSelector((state) => state.player.player);
    const [data, setData] = useState([]);
    useEffect(() => {
        dispatch(BatsManData());

    }, [dispatch]);

    useEffect(() => {
        setData(player);
    }, [player]);

    console.log(player);
    console.log(data);

    return (
        <div>
            <PlayerList data={data} />
        </div>
    );
}

export default BatsMan;