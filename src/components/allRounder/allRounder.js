import React, {useState,useEffect } from "react";
import { AllrounderData } from "../../actions";
import { useSelector,useDispatch } from "react-redux";
import PlayerList from "../../pages/playerList";


const Allrounder = () => {
    const dispatch = useDispatch();
    const player = useSelector((state) => state.player.player);
    const [data, setData] = useState([]);

    useEffect(() => {

        dispatch(AllrounderData());
        
    }, [dispatch]);

    useEffect(() => {
        setData(player);
    }, [player]);

    console.log(player);
    console.log(data);

    return (
        <div>
            <PlayerList data={data} />
        </div>
    );
}

export default Allrounder;