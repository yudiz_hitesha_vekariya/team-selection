import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

const PlayerList = (props) => {
  const [player, setPlayer] = useState([]);

  useEffect(() => {
    setPlayer(props.data);
  }, [props.data]);

  console.log(player);

  return (
    <>
      {props.data.map((data, index) => {
        return (
          <div key={index} className={data.iMatchId}>
            <p key={data.iPlayerId}>{data.sName}</p>
            <p>{data.sTeamName}</p>
            <img src={data.sLogoUrl} alt="img" />
            <p>{data.nScoredPoints}</p>
            <p>{data.nFantasyCredit}</p>
            <p>{data.nSetBy}</p>
          </div>
        );
      })}
    </>
  );
};
PlayerList.propTypes = {
  data: PropTypes.any,
};

export default PlayerList;
