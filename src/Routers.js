import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Allrounder from "./components/allRounder/allRounder";
import BatsMan from "./components/batsman/batsMan";
import Bowler from "./components/bowler/bowler";
import Mainpage from "./components/mainpage/Mainpage";
import WicketKeeper from "./components/wicketKeeper/Wicketkeeper";

const Routers = () => {
  return (
    <BrowserRouter>
      <Mainpage />
      <Routes>
        <Route exact path="/" element={<WicketKeeper />} />
        <Route exact path="/allr" element={<Allrounder />} />
        <Route exact path="/bats" element={<BatsMan />} />
        <Route exact path="/bwl" element={<Bowler />} />
      </Routes>
    </BrowserRouter>
  );
};

export default Routers;
