import React from "react";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Allrounder from "./components/allRounder/allRounder";
import BatsMan from "./components/batsman/batsMan";
import Bowler from "./components/bowler/bowler";
import Mainpage from "./components/mainpage/Mainpage";
import WicketKeeper from "./components/wicketKeeper/Wicketkeeper";

function App() {
  return (
    <div className="maindata">
      <BrowserRouter>
        <div>
          <Mainpage />
          <Routes>
            <Route path="/" exact element={<WicketKeeper />} />
            <Route path="/allr" exact element={<Allrounder />} />
            <Route path="/bats" exact element={<BatsMan />} />
            <Route path="/bwl" exact element={<Bowler />} />
          </Routes>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
