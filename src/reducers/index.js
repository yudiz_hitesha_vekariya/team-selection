import { combineReducers } from "redux";
import  playerReducer from "./PlayerReducer";


export default combineReducers({
    player: playerReducer,    
});