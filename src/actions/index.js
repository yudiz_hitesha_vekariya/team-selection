import player from "../apis/playerData";

export const BatsManData = () => async (dispatch) => {
    const response = await player.get();
    const final = response.data.data.matchPlayer;
    dispatch({ type: "BATSMAN", payload: final });
}

export const BowlerData = () => async (dispatch) => {
    const response = await player.get();
    const final = response.data.data.matchPlayer;
    dispatch({ type: "BOWLER", payload: final });
}

export const WicketKeeperData = () => async (dispatch) => {
    const response = await player.get();
    const final = response.data.data.matchPlayer;
    dispatch({ type: "WICKETKEEPER", payload: final });
}

export const AllrounderData = () => async (dispatch) => {
    const response = await player.get();
    const final = response.data.data.matchPlayer;
    dispatch({ type: "ALLROUNDER", payload: final });
}

